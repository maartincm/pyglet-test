import pyglet
from pyglet.window import key

RESOLUTION_X = 1920
RESOLUTION_Y = 1080
FPS = 120


class PhysicalObject(pyglet.sprite.Sprite):

    def __init__(self, *args, **kwargs):
        super(PhysicalObject, self).__init__(*args, **kwargs)
        self.velocity_x, self.velocity_y = 0.0, 0.0


class gameObject(PhysicalObject):

    def __init__(self, path=False, *args, **kwargs):
        img = pyglet.resource.image(path)
        super(gameObject, self).__init__(img=img, *args, **kwargs)


# TODO
class Player(gameObject):

    def __init__(self, *args, **kwargs):
        pass


class Ball(gameObject):

    def __init__(self, *args, **kwargs):
        path = 'Assets/ball.png'
        super(Ball, self).__init__(path=path, *args, **kwargs)
        self.velocity_x = 300
        self.velocity_y = 300
        self.x_dir = 1
        self.y_dir = 1

    def update(self, dt):
        self.x += self.velocity_x * dt * self.x_dir
        self.y += self.velocity_y * dt * self.y_dir
        self.check_bounds()

    def check_bounds(self):
        min_x = 0
        min_y = 0
        max_x = RESOLUTION_X - self.image.width
        max_y = RESOLUTION_Y - self.image.height
        if self.x <= min_x:
            self.x_dir = 1
        elif self.x >= max_x:
            self.x_dir = -1
        if self.y <= min_y:
            self.y_dir = 1
        elif self.y >= max_y:
            self.y_dir = -1


class Game:

    def __init__(self, win, *args, **kwargs):
        self.win = win
        self.win.set_fullscreen(True)

        self.toggle_fullscreen = False
        self.keys = key.KeyStateHandler()
        self.win.push_handlers(self.keys)

        self.background = pyglet.graphics.Batch()
        self.bg_image = gameObject('Assets/black_background.png',
                                   batch=self.background)
        self.ball = Ball(x=500, y=500)
        self.game_objects = [self.bg_image, self.ball]

    def update(self, dt):
        self.check_key_combs()
        for obj in self.game_objects:
            obj.update(dt)

    def check_key_combs(self):
        if self.keys[key.LALT] and self.keys[key.F4]:
            pyglet.app.exit()
        if self.keys[key.F11]:
            if not self.toggle_fullscreen:
                self.toggle_fullscreen = True
                self.win.set_fullscreen(not self.win.fullscreen)
        else:
            self.toggle_fullscreen = False


class Window(pyglet.window.Window):

    def __init__(self, *args, **kwargs):
        super(Window, self).__init__(*args, **kwargs)
        self.label = pyglet.text.Label('What a game!')

    def on_draw(self):
        self.clear()
        self.label.draw()
        for obj in self.game.game_objects:
            obj.draw()


if __name__ == '__main__':
    win = Window()
    game = Game(win)
    win.game = game
    pyglet.clock.schedule_interval(game.update, 1.0/FPS)
    pyglet.app.run()
